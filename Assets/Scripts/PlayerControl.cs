﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    float horizontal;
    Rigidbody2D rb;
    Animator anim;
    float speed = 3f;
    float jumpForce = 4f;
    bool isJump = false;
    bool suelo = false;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        horizontal = Input.GetAxis("Horizontal");

        if (Input.GetKeyDown(KeyCode.Space) && suelo)
        {
            isJump = true;
        }

        anim.SetBool("suelo",suelo);
    }

    private void FixedUpdate()
    {
        //Desplazamiento horizontal
        if (Mathf.Abs(horizontal)>0.01f)
        {
            rb.velocity = new Vector2(horizontal*speed, rb.velocity.y);
        }
        //Flip de personaje (volteado izq-der)
        if (horizontal > 0 && Mathf.Abs(horizontal)>0.2)
        {
            transform.localScale = new Vector3(-1,1,1);
        }
        else if(horizontal < 0 && Mathf.Abs(horizontal) > 0.2)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }

        if (isJump)
        {
            rb.AddForce(Vector2.up * jumpForce,ForceMode2D.Impulse);
            isJump = false;
        }

        anim.SetFloat("speed", Mathf.Abs(rb.velocity.x));
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Plataforma"))
        {
            suelo = true;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Plataforma"))
        {
            suelo = false;
        }
    }
}
